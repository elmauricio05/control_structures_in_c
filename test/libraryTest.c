#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "jni_test.h"
#include "library.c"

/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es) 
 * @version 1.0
 */

/////////////////////////////////////////////////////////
// -------------------- maximum ---------------------- //
/////////////////////////////////////////////////////////
void testMaximum_7_5() {
    // Given
    int mayor;
    // When
    mayor = maximum(7,5);
    // Then
    assertEquals_int(7, mayor);
}

void testMaximum_3_9() {
    int mayor;
    // When
    mayor = maximum(3,9);
    // Then
    assertEquals_int(9, mayor);
}

void testMaximum_4_4() {
    // Given
    int mayor;
    // When
    mayor = maximum(4,4);
    // Then
	assertEquals_int(4, mayor);
}
	
	
/////////////////////////////////////////////////////////
// -------------------- sumUpTo ---------------------- //
/////////////////////////////////////////////////////////
	
void testSumUpTo_minus1() {
    // Given
    int suma;
    // When
    suma = SumUpTo(-1);
    // Then
	fail("Not yet implemented");
}

void testSumUpTo_0() {
    // Given
    
    // When
    
    // Then
	fail("Not yet implemented");
}

void testSumUpTo_1() {
    // Given
    
    // When
    
    // Then
	fail("Not yet implemented");
}

void testSumUpTo_3() {
    // Given
    
    // When
    
    // Then
	fail("Not yet implemented");
}

void testSumUpTo_7() {
    // Given
    
    // When
    
    // Then
	fail("Not yet implemented");
}

	
/////////////////////////////////////////////////////////
// ---------------------- sum ------------------------ //
/////////////////////////////////////////////////////////
	
void testSum_Empty() {
    // Given
    
    // When
    
    // Then
	fail("Not yet implemented");
}

void testSum_5() {
    // Given
    
    // When
    
    // Then
	fail("Not yet implemented");
}

void testSum_5_7() {
    // Given
    
    // When
    
    // Then
	fail("Not yet implemented");
}

void testSum_3_minus4_8() {
    // Given
    
    // When
    
    // Then
	fail("Not yet implemented");
}

void testSum_minus2_5_3_minus1_9() {
    // Given
    
    // When
    
    // Then
	fail("Not yet implemented");
}

void testSum_9_minus8_15_6_minus10_7_4() {
    // Given
    
    // When
    
    // Then
	fail("Not yet implemented");
}
