/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

#include <stdlib.h>

#if defined(_WIN32) || defined(_WIN64) || defined(__CYGWIN__)
    #define LibraryFrame "java -cp \"src;test;.jni;.jar/org.junit_4.13.2.v20211018-1956.jar;.jar/org.hamcrest.core_1.3.0.v20180420-1519.jar;.jar/rsyntaxtextarea-3.3.3.jar;.jar/LibraryFrame.jar\" LibraryFrame"
#else
    #define LibraryFrame "java -cp \"src:test:.jni:.jar/org.junit_4.13.2.v20211018-1956.jar:.jar/org.hamcrest.core_1.3.0.v20180420-1519.jar:.jar/rsyntaxtextarea-3.3.3.jar:.jar/LibraryFrame.jar\" LibraryFrame"
#endif

int main(void) {
	system(LibraryFrame);
	return 0;
}
