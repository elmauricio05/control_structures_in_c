/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */
public class library {
    public native int maximum(int left, int right);
    public native int sumUpTo(int end);
    public native int sum(int[] value, int valueLength);

    static {
        System.load(new java.io.File(".jni", "library_jni.so").getAbsolutePath());
    }
}
