# El comando include se utiliza para incluir archivos de makefile y produce un error 
# si el archivo no se encuentra, mientras que -include también se utiliza para incluir 
# archivos de Makefile, pero no genera un error si el archivo no se encuentra.
include .jni/makefile

.PHONY: all clean

all: run/LibraryFrame

clean:
	rm -rf run/*
	rm -f .jni/*.o .jni/*.so .jni/*.class
